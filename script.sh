#!/bin/bash
MYVAR=$(./main $1 $2 $3 $4 $5 407 468)
arrIN=(${MYVAR})

echo ${arrIN[0]}
echo ${arrIN[1]}

$(convert compass.png -fill red -stroke black -strokewidth 2 -draw "circle ${arrIN[1]},${arrIN[0]} ${arrIN[1]},$(echo ${arrIN[0]} + 15 | bc)" compass2.png)

if [[ $# -gt 5 ]]; then
	$(mv compass2.png $6.png)
fi
