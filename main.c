#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//these values were found using the top players spreadsheet
double a = 5.915912247;
double b = 0.023963709;
double c = 11.99408775;

// the first vlaue = mininum
// the D value = difference to higheest (first value + D = max value)
// I got most of thees from the aimer7 guide
double w6t = 165;
double w6tD = 35;
double b180 = 70;
double b180D = 25;

double clsi = 14000;
double clsiD = 4000;
double air = 820;
double airD = 25;

double scale = 2;
double offset = 0;


double inv(double y){
	double x =log((y-c)/a)/b;
	x = 50*sin((x-50)/(100/M_PI))+50;
	x = 50*sin((x-50)/(100/M_PI))+50;
	return x/100;
}


//user1 to 4 are the difefrent scores for b180, clsi, etc
// NOTE: the two click timing are first, then the two tracking
double ratio(double user1, double user2, double user3, double user4){
	double click = ((user1-w6t)/w6tD+(user2-b180)/b180D)/2;
	double track = ((user3-clsi)/clsiD + (user4-air)/airD )/2;
	return track/(track+click) - 0.5;
}

int main(int argc,char* argv[]) {

	if(argc==1){
		printf("Needs arguements: Sens, 1wall6targets TE score, Bounce 180 Score, Close Long Strafes Invincible score, air score\n");
		return 0;
	}
	if(argc<6){
		printf("Error; expected 5 arguements but recieved less\n");
		return 0;
	}
	if(argc>6){
		scale = 2*atof(argv[6]);
	}
	if(argc>7){
		offset = atof(argv[7]);
	}

	double sens = atof(argv[1]);
	printf("%f\n",scale*(inv(sens)-0.5)+offset);
	double outratio = ratio(atof(argv[2]),atof(argv[3]),atof(argv[4]),atof(argv[5]));
	printf("%f\n", scale*outratio+offset);
	return 0;
}
